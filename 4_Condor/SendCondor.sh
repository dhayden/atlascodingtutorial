#!/bin/bash
color='\e[1;32m'
NC='\e[0m'

mkdir Folders
cd Folders
Path=$(pwd)

INDEX=( 1 2 3 4 5 6 7 8 9 10 ) #i

for i in {0..9}
do
    # Initial setup for each job
    File=${INDEX[i]}
    echo -e "${color}Preparing Sample: ${File}"
    mkdir ${File}
    cd ${File}
    
    # For submission script 
    echo "Universe = vanilla" 			       > condor.submit
    echo "getenv = true" 			       >> condor.submit
    echo "Executable = ${Path}/${File}/RunCondor.sh"   >> condor.submit
    echo "Input=/dev/null"			       >> condor.submit 
    echo "Output=${Path}/${File}/condor.stdout"	       >> condor.submit 
    echo "Error=${Path}/${File}/Error.sterr" 	       >> condor.submit
    echo "Log =${Path}/${File}/condor.log"             >> condor.submit 
    echo "+IsBypassJob = True "                        >> condor.submit 
    echo "Concurrency_Limits = DISK_T3WORK1:100"       >> condor.submit 
    echo "Queue" 			       	       >> condor.submit
		
    # For condor script
    echo "#!/bin/bash" 					       > RunCondor.sh
    echo "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase" >> RunCondor.sh
    echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -2" >> RunCondor.sh
    echo "asetup AthGeneration 23.6.13"                        >> RunCondor.sh
    echo "cp -r ${Path}/../Run.sh ."			       >> RunCondor.sh
    echo "sed -i 's/REPLACE/'${INDEX[i]}'/g' Run.sh"   	       >> RunCondor.sh
    echo "chmod u+x Run.sh"				       >> RunCondor.sh
    echo "source Run.sh"				       >> RunCondor.sh
    echo "chmod 777 -R ."                                      >> RunCondor.sh
		
    # Submit to condor
    chmod u+x RunCondor.sh
    condor_submit condor.submit
    
    # Returns to original directory 
    cd ../
done
cd .. 
