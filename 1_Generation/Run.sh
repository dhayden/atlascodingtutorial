# Setup the ATLAS coding environment
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup 19.2.5.11.2,MCProd,64,here

# These are "static" configurable parameters
COM_ENERGY=13000
RUN_NUMBER=999999
START_EVENT=1
NUMBER_EVENTS=10000
RANDOM_SEED=1234
OUTPUT_NTUPLE=EVNT.root

# Command line parameters
INPUT_JOBOPTIONS=MC15.999999.Pythia8EvtGen_A14MSTW2008LO_Zprime_NoInt_ee_SSM3000.py
OUTPUT_DIRECTORY=.

mkdir $OUTPUT_DIRECTORY

echo "I am a job"
echo "I am running in directory $OUTPUT_DIRECTORY"
echo "EvgenCorrAtlfast_trf.py $COM_ENERGY $RUN_NUMBER $START_EVENT $NUMBER_EVENTS $RANDOM_SEED $INPUT_JOBOPTIONS $OUTPUT_FILE $OUTPUT_AOD $OUTPUT_NTUPLE"

# Delete old files from a possible previous run
rm -fr EVNT.root DAOD_TRUTH0.TRUTH.root

# Run the Pythia 8 Generation
Generate_tf.py --ecmEnergy $COM_ENERGY --runNumber $RUN_NUMBER --firstEvent $START_EVENT --maxEvents $NUMBER_EVENTS --randomSeed $RANDOM_SEED --jobConfig $INPUT_JOBOPTIONS --outputEVNTFile $OUTPUT_NTUPLE

# Do some more setup for the next stage which turns these generated events into a format that can be read by ATLAS code.
asetup Athena,21.0.136,here

# Make EVNT into TRUTH file
Reco_tf.py --inputEVNTFile EVNT.root --outputDAODFile TRUTH.root --reductionConf TRUTH0
