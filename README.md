# Introduction

A tutorial aimed at helping new Graduates and even some Undergraduates doing summer research, get used to the ATLAS coding frameworks. This tutorial will not be exhaustive on the learning aspects, but give you enough information to understand the motivation behind the code you will run, and continue further with your own reading.

# Pre-requisites

- Git access.
- MSU Tier-3 computing account.

Log into the Tier-3 computing cluster:

    ssh -Y username@pandeia.aglt2.org

Move to one of the work disks which have a lot of free space, i.e.

    cd /msu/data/t3work10

Create your own directory to work from (usually your username), and move to that directory:

    mkdir username
    cd username

Clone this repository and move into that directory:

    git clone ssh://git@gitlab.cern.ch:7999/dhayden/atlascodingtutorial.git
    cd atlascodingtutorial

You're now ready to begin!

# ATLAS Coding Tutorial

The sub-sections below will guide you through the tutorial, pointing to short Youtube Videos for understanding behind the steps, followed by coding sections of the tutorial before moving onto the next stage. Feel free to ask Dan Hayden questions at any point via e-mail for clarifications or issues.

Table of Contents
1. [Intro: ATLAS and the LHC](#Intro)
2. [Event Generation](#Evgen)
3. [Truth MC Analysis](#TruthAna)
4. [Real Analysis](#Real)
5. [Event Selection](#Select)
6. [MSU Tier-3 Computing](#MSUT3)
7. [Condor](#Condor)
8. [Video Appendix](#Vids)

## Intro: ATLAS & The LHC <a name="Intro"></a>

First start by watching the following video:

[Intro: ATLAS & The LHC](https://www.youtube.com/watch?v=Q2Sw1GVdReM) (12:43)

You can then either go onto the next section, or watch another video with some background theory information:

[Intro: Theory](https://www.youtube.com/watch?v=0AZb5egOFB0) (10:21)

## Event Generation <a name="Evgen"></a>

To generate some Z' signal events from proton-proton collisions at the LHC, first move into the following directory:

    cd 1_Generation

Within this directory you will see two files:

    MC15.999999.Pythia8EvtGen_A14MSTW2008LO_Zprime_NoInt_ee_SSM3000.py
    Run.sh

The first file is a so-called "JobOption" this tells the Monte-Carlo (MC) generator - in this case Pythia 8 - what physics process to generate. The naming of the file tells you most of the information you need to know

- MC15: MC Campaign from 2015.
- 999999: This is the dataset identification number (DSID). Each physics process in ATLAS has a unique ID, and this also goes for real datasets, which have a unique 6 digit number to identify them.
- Pythia8EVtGen: This tells you the generator which is being used.
- A14MSTW2008LO: This tells you the parton distribution function (PDF) thay is being used, along with the ATLAS tune. This is basically the underlying estimate about the structure of the proton that is being assumed.
- Zprime_NoInt: This is the physics process we are generating, in this case with no interference between the SM and BSM process.
- ee: The decay channel for the signal, in this case to two electrons.
- SSM3000: The BSM theory model (in this case the sequential standard model (SSM)), following by the assumed mass of the new heavy particle, in this case 3000 GeV.

If you look inside this file you will see a number of technical settings that instruction the generator what to do when this jobOption is used. The second file is a script that will setup the ATLAS coding environment and then generate some events using the jobOption we just looked at. If you open Run.sh you will see a number of features, which are explained with comments in the code.

Essentially this file is going to generate 10,000 proton-proton collisions at a center of mass energy of 13 TeV, which all result in a Z' particle being created at a mass of 3 TeV, and subsequently decay to two electrons. This output is then further converted into a so-called "Truth" format that can be read by some standard ATLAS code. To run this script simple do:

    ./Run.sh

You will know the code has worked correctly if when the script finishes (will take ~15 mins at most) among many files in your directory you should see at least:

    EVNT.root
    DAOD_TRUTH0.TRUTH.root

If the run is successful then proceed to watching the next video which will review what you've done and guide you onto the next stage of the tutorial:

## Truth MC Analysis <a name="TruthAna"></a>

Start by watching this video:

[Truth MC Analysis](https://www.youtube.com/watch?v=sx-YN9v2VcU) (15:04)

Now that you have a sample of Z' events which are in an ATLAS readable format, we should move into the next directory which will analyse those events:

    // Assuming that you are still in the directory where we left off
    cd ../2_AnalyseTruthMC

Within this directory there are a number of files. Again we have a code setup file called Setup.sh, and a Run.sh file which will compile and Run the analysis code. But first let's take a look at the analysis code which will run over the events we just produced: MC15Validation.C. There are comments in the code which should guide you through what each step is doing.

After reading through the file, you can start by checking that the code runs for you "out of the box" by doing the following commands:

    source Setup.sh
    ./Run.sh

Then, assuming that a file called "Histos.root" is created after running these commands, you can look at the histograms you just made by doing:

    root -l
    new TBrowser
    // Then navigate in the browser to the Histos.root file, and click on the histograms within the file to see the plotted information.
    // To close the browser do:
    .q

Now, before moving onto the next section, let's try to modify the code and see that your changes go into effect. Looking at the examples in the file already, i'd like you to add information about the "phi" angular direction of the Z' particle and both of its decay products. Remember that you will need to:

- Book new histograms (tip: for phi it would be reasonable for the x-axis to go from -3.15 to +3.15, and to use 200 bins).
- Fill those new histograms with the "Phi" variable, which you can ask selected objects for.
- Save those histograms to the output file.

Once you've made those changes, there is no need to do the setup again unless you close your terminal, so you can just make sure the previous .root file is deleted and run the code again:

    rm Histos.root
    ./Run.sh

Use the ROOT TBrowser again to check that these new histograms have been added to the newly created Histos.root, and that there are a number of entries filled. If you want to go one step even further, why don't you try to add a cut to the analysis, in the form of an if-statement that only fills the Z' mass histogram if the mass is higher than 3000 GeV. This is not something that we'd really want to do in the analysis, but would be a good opportunity for you to try and further modify the code based on your experience so far, and so see what the mass histogram would look like in this case - ask questions if you get stuck!

Finally you can even make images of some of the variables using the Histos.root file you just created. This is done so that plots can be shown in presentations and made publication worthy. The macro MakePlot.C will take the Histos.root file and make a nice plot called TruthMCPlot.eps. You can take a look inside the .C file for instructions on each step. To run the macro you can just do:

    root -l
    .x MakePlot.C
    // Exit ROOT with:
    .q

So there you have it! You've analysed some truth MC and made a nice plot. Watch the next video for a round up, and learn about the next steps towards a full and more realistic analysis.

## Real Analysis <a name="Real"></a>

This section will make the leap to a real analysis that applies to both simulation and real data. The following video provides the motivation behind this complexity:

[Real Analysis](https://www.youtube.com/watch?v=sw3j1mYwfI8) (22:07)

## Event Selection <a name="Select"></a>

This section takes a more in depth look at how an analysis selects events for study and the reasoning behind the various criteria.

[Event Selection](https://youtu.be/9SBvsiF0G4g) (40:54)

## MSU Tier-3 Computing <a name="MSUT3"></a>

The MSU Tier-3 (T3) Computing system consists of several login nodes which you can access via ssh (ssh -Y USERNAME@LOGINNODE.aglt2.org). The various login nodes and their features are:

- "ersa" and "pandeia": These are the main current login nodes.
- "alpheus" and "heracles": Slightly older hardware, but just fine.
- "green" and "maron": Our AVX enabled nodes ideal for interatively testing AVX/ML applications. Maron has slightly more cores, and both have plenty of memory.

Once logged in you will land in your home area, which is backed up daily. For this reason we recommend only keeping O(10 GB) of files in this area, and only those that you critically want backed up. This could consist of important code, instructions not stored elsewhere, etc. Critically, this DOES NOT include datasets and MC samples, which are very large, easily replacable (with time) and thus best stored elsewhere, such as the areas discussed next. The MSU T3 also provides a number of work disks, which everyone has permission to change directory to, create folders, and store any kind of files they like without space restrictions. The caveat is that these work disks are NOT backed up explicitly, though the servers are setup in a raid array to avoid losses due to disk failure. To access one of the work disks once logged in, from anywhere you can do:

cd /msu/data/WORKDISKNAME

Where WORKDISKNAME is one of the many available work disks. To choose a work disk you can use the following webpage to see how big each disk is and how much space is currently free. By clicking on a given work disk link on the webpage you can see what space is being used and by whom:

https://web.pa.msu.edu/people/laurens/msut3/workspace/summary.html

Note that there is a username and password for this webpage, please ask Dan Hayden or Philippe Laurens if you don't know it. Two quick useful notes here:

- On any given work disk one usually first creates a folder with your username as the name, and then within that folder you can make any directory structure you like. This keeps everyones work cleanly separated.
- If you "ls" the /msu/data directory you might sometimes only see a subset of the disks. This DOES NOT mean that the other disks are unavailable, merely that you are not currently using them and this they are not mounted in your session. If you know the disk exists you can still "cd" to it, even if the name is not visible at first.

If you notice any slow down on the login nodes, please contact Dan Hayden and Philippe Laurens asap with details of the symptoms and we'll try to get them resolved quickly. Please also do not run many processes interactively from the command line, especially memory intensive ones, as this can slow down the login node for everybody. If you need to run many jobs in parallel, you have the condor system available to you, as discussed in the next chapter.

## Condor <a name="Condor"></a>

The MSU Condor system consists of around 1000 worker nodes, meaning that a total of 1000 analysis jobs can be run in parallel on the MSU Tier-3 computing system. This section provides a script to run condor out of the box and complete a very simple task. From this one can modify the script to do more complex tasks, essentially running whatever you would usually launch locally. To run the example, logon to the Tier-3, clone the tutorial repository and do:

    cd 4_Condor
    ./SendCondor.sh

The script launches 10 jobs to condor. Firstly for each job it creates a condor.submit file which contains the environment information that condor needs to know for running the job. Secondly for each job it creates a RunCondor.sh file which contains the specific instructions for that job, as if it were being run directly on the command line. Some useful commands have been included like sed commands to replace parts of files. Run.sh is a script that nominally does not change from job to job (though it can) and contains what we want executed in the job. Some other useful commands in the script are:

Job queue length:

    +isShortJob # time limit < 3 hours.
    +isMediumJob # time limit < 2 days.
    +isLongJob # time limit < 1 week.
    +isBypassJob # No time limit (only use if you know your jobs run okay).

Concurrency limits:

You may wish to launch a large number of jobs, but only have i.e. 100 of them run simultaneously, with the rest queued. Concurrency limits helps you do this and can be used with the line:

    Concurrency_Limits = DISK_T3WORK1:100

Here the DISK_T3WORK1 can be any work disks name, and is a token system shared with anyone else using that same name. The 100 then sets how many jobs will be run simultaneously according to the equation NJobs = 10000 / X. In this case X = 100, therefore NJobs = 100. If you set DISK_T3WORK6:200 as another random example, then X = 200, meaning NJobs = 50 (this 50 jobs would run simultaneously.

Finally there are a number of command line arguments which will held you keep track of your jobs:

    condor_status # list the number of available condor nodes, and the number being used.
    condor_q # list the queue of all current condor jobs from all users.
    condor_q -submitter username # list all of the jobs submitted by the user
    condor_release # release any held jobs
    condor_rm 123456 # stop and delete the condor job (or series of jobs) with the ID=123456. You can find the ID from the other condor commands.

It is also useful to know the various job status' you may see and what they mean:

    Idle (I) # the jobs have not started yet.
    Running (R) # the jobs are running.
    Held (H) # the jobs are held (may have exceeded queue time or run out of memory).
    Cancelling (X) # the jobs are in the process of being deleted / removed.    

## Video Appendix <a name="Vids"></a>

This section contains all of the videos for quick reference:

- [Intro: ATLAS & The LHC](https://www.youtube.com/watch?v=Q2Sw1GVdReM) (12:43)
- [Intro: Theory](https://www.youtube.com/watch?v=0AZb5egOFB0) (10:21)
- [Truth MC Analysis](https://www.youtube.com/watch?v=sx-YN9v2VcU) (15:04)
- [Real Analysis](https://www.youtube.com/watch?v=sw3j1mYwfI8) (22:07)
- [Event Selection](https://youtu.be/9SBvsiF0G4g) (40:54)